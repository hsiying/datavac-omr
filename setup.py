"""Setup for the MRR-OMR"""
# from distutils.core import setup
import os
import re
import codecs
from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))


def read(*parts):
    """
    Read file

    Returns:
        str: content of the file.
    """
    with codecs.open(os.path.join(here, *parts), 'r') as fp:
        return fp.read()


def find_version(*file_paths):
    """
    Find version from __init__.py

    Raises:
        RuntimeError: if not found

    Returns:
        str: version number
    """
    version_file = read(*file_paths)
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")


with open("README.md", "r") as fh:
    long_description = fh.read()

setup(name='mrr-omr',
      version=find_version('omr', '__init__.py'),
      license='MIT',
      description='Reading medical record marks from a photo.',
      long_description=long_description,
      long_description_content_type="text/markdown",
      author='S.Huang',
      author_email='data.vac.notificationl@gmail.com',
      packages=find_packages(),
      python_requires='>=3.7',
      install_requires=['opencv-contrib-python >= 4.0',
                        'numpy',
                        'imutils >= 0.5.3',
                        'matplotlib',
                        'scipy'])
