import logging
from datetime import datetime

logger = logging.getLogger('OMR')
formatter = logging.Formatter(
    '%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
handler_s = logging.StreamHandler()
handler_s.setFormatter(formatter)

handler_f = logging.FileHandler(
    f'omr_{datetime.now().strftime("%Y-%m-%d_%H-%M-%S")}.log')
handler_f.setFormatter(formatter)

logger.addHandler(handler_s)
logger.addHandler(handler_f)
logger.setLevel(logging.DEBUG)