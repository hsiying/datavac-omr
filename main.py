import numpy as np
import cv2
from omr_logger import logger
# from datetime import datetime
from collections import deque
# import os
from omr.utils import ImageQualityError
from omr.improcess import img_scale
from omr.read_pic import FindPaper, low_resolution, uneven_tone
from omr.contour_ops import  FindContour, SortContour, FilterContour
from omr.multiple_choice import  ReadMarks
from omr.implot import plot_contour, plot_contour_order, plot_image


def import_image(imgfile):
    logger.info('Importing image...')
    return cv2.imread(imgfile)


def extract_form(photo):
    logger.info('Finding the form from the image.')
    logger.info(
        f'The image resolution in pixels: {photo.shape[0] , photo.shape[1]}')
    p = FindPaper(photo).gray_blur_edge(bright=30, contrast=30, canny_l=105, canny_h=210, q=(11, 11), sig=0, show_img=False)\
        .thresholding().find_contour('edge', 'external', return_self=True).boundry()
    form = p.paper
    logger.info(f'paper dimensions: {form.shape}')
    return form


def check_resolution(form):
    if low_resolution(form):
        logger.warning(f'image resolution: {photo.shape}')
        raise ImageQualityError(
            'The resolution is too low.Please change a picture with high resolution.')


def set_parameters(form):
    scale = img_scale(form, [1530, 2459])
    logger.debug(f'scale: {scale :.2f}')
    if scale < 0.7:
        sect_sig = 5
        mark_q = (9, 9)
        logger.debug(
            f'parameters to be used: sect_sig: {sect_sig}, mark_q: {mark_q}')
    else:
        sect_sig = 20
        mark_q = (25, 25)
        logger.debug(
            f'parameters to be used: sect_sig: {sect_sig}, mark_q: {mark_q}')

    def reset_q(org_q=mark_q):
        return org_q
    return mark_q, sect_sig, reset_q


def preprocess(form):
    logger.info('Preprocessing image...')
    image = cv2.cvtColor(form, cv2.COLOR_BGR2GRAY)
    th = cv2.adaptiveThreshold(
        image, 100, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 7, 5)
    return th


def find_section_contours(th, sect_sig):
    while True:
        try:
            p = FindContour(th).gray_blur_edge(contrast=-110, bright=-120, q=(15, 15), sig=sect_sig)\
                .thresholding().find_contour('otsu', 'ccomp', return_self=True)
            cnt, hier = p.cnts, p.hier
            ext_cnts, ext_hier = FilterContour(cnt, hier).filter_layer()
            rect_cnts, rect_hier = FilterContour(
                ext_cnts, ext_hier).by_n_edge(frac=0.01)

            ref_size = [45000., 150000.]
            sect_cnts = FilterContour(rect_cnts, rect_hier).by_area_size(
                10000, 5500000., verbose=True)

            logger.debug(
                f'Number of section box contour detected: {len(sect_cnts)}')

            if len(sect_cnts) < 12:
                raise ValueError("Incorrect number of contours, keep finding")
            sorted_cnts = SortContour(sect_cnts).sort_centroid(
                tol=10).match_contour()
            # plot_contour(th, sorted_cnts)
        except:
            sect_sig -= 1
            logger.debug(f'next sect_sig value: {sect_sig}')
            if sect_sig <0:
                logger.error("Can't find the right parms.")
                break
        else:
            logger.debug(f'The sect_sig value worked: {sect_sig}')
            break
    return sorted_cnts


def read_section5(th, mark_q):
    logger.info('Start reading section 5...')
    q_labels = ['Comorbiditie '+str(i) for i in range(1, 13, 1)]
    comorbids = {i: 'NA' for i in q_labels}
    while True:
        try:
            rest = ReadMarks(th, sorted_cnts, 5).\
                gray_blur_edge(contrast=-100, bright=0, sig=10, q=mark_q, show_img=False)\
                .thresholding()\
                .find_contour('otsu', 'ccomp', return_self=True)\
                .filter_mark_contours(sample_dim=[784, 511], shape=MARK_SHAPE, ratio=MARK_RATIO,
                                      area=[670., 125, 670., 235.], debug=True, show_contour=False)\
                .read_box_answers(ncol=3, nrow=12, a_label={0: 1, 1: 0, 2: -995},
                                  q_label=q_labels)
        except:
            q = mark_q[0]
            q -= 2
            mark_q = (q, q)
            logger.debug(f'Next lookup value of mark_q: {mark_q}')
            if q < 0:
                logger.debug(
                    f"Can't fine the rigt parameters. Values return to NA.")
                mark_q = reset_q()
                break
        else:
            logger.debug(f'Final value of mark_q worked: {mark_q}')
            comorbids.update(rest)
            break
    return comorbids


def read_section2(th, mark_q):
    logger.info('Start reading section 2...')
    q_labels = ['Alcohol', 'Smoking', 'health worker']
    risk_factors = {i: 'NA' for i in q_labels}
    while True:
        try:
            rest = ReadMarks(th, sorted_cnts, 2)\
                .gray_blur_edge(bright=0, contrast=0, sig=0, q=mark_q, show_img=False)\
                .thresholding()\
                .find_contour('otsu', 'ccomp', return_self=True)\
                .filter_mark_contours(sample_dim=[481, 676], shape=MARK_SHAPE, area=[450., 210, 600., 350.], ratio=MARK_RATIO, debug=True, show_contour=False)\
                .read_box_answers(ncol=2, nrow=3, a_label={0: 1, 1: 0}, q_label=q_labels)
        except:
            q = mark_q[0]
            q -= 2
            mark_q = (q, q)
            logger.debug(f'Next look up value for mark_q: {mark_q}')
            if q < 0:
                logger.debug(
                    f"Can't fine the rigt parameters. Values return to NA.")
                mark_q = reset_q()
                break
        else:
            logger.debug(f'Final value worked for mark_q: {mark_q}')
            risk_factors.update(rest)
            break
    return risk_factors


def read_section7(th, mark_q):
    logger.info('Start reading section 7...')
    q_labels = ['Symptom '+str(i) for i in range(1, 13, 1)]
    symptoms = {i: 'NA' for i in q_labels}
    while True:
        try:
            # had problem in this area for min contour size  of 14
            rest = ReadMarks(th, sorted_cnts, 7)\
                .gray_blur_edge(contrast=0, bright=0, sig=0, q=mark_q, show_img=False)\
                .thresholding()\
                .find_contour('otsu', 'ccomp', return_self=True)\
                .filter_mark_contours(sample_dim=[1306, 674], shape=MARK_SHAPE, area=[1100., 205, 600., 350.], ratio=MARK_RATIO, debug=True, show_contour=False)\
                .read_box_answers(ncol=2, nrow=12, a_label={0: 1, 1: 0}, q_label=q_labels)
        except:
            q = mark_q[0]
            q -= 2
            mark_q = (q, q)
            logger.debug(f'Next lookup value of mark_q: {mark_q}')
            if q < 0:
                logger.debug(
                    f"Can't fine the rigt parameters. Values return to NA.")
                mark_q = reset_q()
                break
        else:
            logger.debug(f'Final value of mark_q worked: {mark_q}')
            symptoms.update(rest)
            break
    return symptoms


def read_section8(th, mark_q):
    logger.info('Start reading section 8...')
    q_labels = ['Item '+str(i) for i in range(1, 13, 1)]
    localization = {i: 'NA' for i in q_labels}
    while True:
        try:
            rest = ReadMarks(th, sorted_cnts, 8)\
                .gray_blur_edge(contrast=0, bright=0, sig=0,  q=mark_q, show_img=False)\
                .thresholding()\
                .find_contour('otsu', 'ccomp', return_self=True)\
                .filter_mark_contours(sample_dim=[1316, 687], shape=MARK_SHAPE, area=[1050., 215, 600., 360.], ratio=MARK_RATIO, debug=True, show_contour=False)\
                .read_box_answers(ncol=2, nrow=12, a_label={0: 1, 1: 0}, q_label=q_labels)
        except:
            q = mark_q[0]
            q -= 2
            mark_q = (q, q)
            logger.debug(f'Next lookup value of mark_q: {mark_q}')
            if q < 0:
                logger.debug(
                    f"Can't fine the rigt parameters. Values return to NA.")
                mark_q = reset_q()
                break
        else:
            logger.debug(f'Final value of mark_q worked: {mark_q}')
            localization.update(rest)
            break
    return localization


def read_section9(th, mark_q):
    logger.info('Start reading section 9...')
    q_labels = deque(['Antibiotics '+str(i) for i in range(1, 11, 1)])
    q_labels.extendleft(['Vasopressor', 'Fuild total:', 'Fluids by Ambulance'])
    q_labels.extend(['no drug therapy'])
    antibio = {i: 'NA' for i in q_labels}
    while True:
        try:
            rest = ReadMarks(th, sorted_cnts, 9)\
                .gray_blur_edge(contrast=0, bright=0, sig=0, q=mark_q, show_img=False)\
                .thresholding()\
                .find_contour('otsu', 'ccomp', return_self=True)\
                .filter_mark_contours(sample_dim=[1317, 815], shape=MARK_SHAPE, ratio=MARK_RATIO, area=[1250., 215, 310., 60.], debug=True, show_contour=False)\
                .read_box_answers(ncol=2, nrow=14, a_label={0: 1, 1: 0}, q_label=q_labels)
        except:
            q = mark_q[0]
            q -= 2
            mark_q = (q, q)
            logger.debug(f'Next lookup value of mark_q: {mark_q}')
            if q < 0:
                logger.debug(
                    f"Can't fine the rigt parameters. Values return to NA.")
                mark_q = reset_q()
                break
        else:
            logger.debug(f'Final value of mark_q worked: {mark_q}')
            antibio.update(rest)
            break
    return antibio


def read_section10(th, mark_q):
    logger.info('Start reading section 10...')
    q_labels = ['Kontroll '+str(i) for i in range(1, 7, 1)]
    control = {i: 'NA' for i in q_labels}
    while True:
        try:
            control_cls = ReadMarks(th, sorted_cnts, 10)\
                .gray_blur_edge(contrast=0, bright=0, sig=0, q=mark_q, show_img=False)\
                .thresholding(thresh_l=0, thresh_h=100).find_contour('otsu', 'ccomp', return_self=True)

            c1 = control_cls.filter_mark_contours(sample_dim=[508, 1403], shape=MARK_SHAPE, area=[430., 215, 600., 360.], debug=True, show_contour=False)\
                .read_box_answers(ncol=2, nrow=3, a_label={0: 1, 1: 0}, q_label=q_labels[:3])

            c2 = control_cls.filter_mark_contours(sample_dim=[508, 1403], shape=MARK_SHAPE, ratio=MARK_RATIO, area=[430., 215, 1350., 1005.], debug=False, show_contour=False)\
                .read_box_answers(ncol=2, nrow=3, a_label={0: 1, 1: 0}, q_label=q_labels[3:])
        except:
            q = mark_q[0]
            q -= 2
            mark_q = (q, q)
            logger.debug(f'Next lookup value of mark_q: {mark_q}')
            if q < 0:
                logger.debug(
                    f"Can't fine the rigt parameters. Values return to NA.")
                mark_q = reset_q()
                break
        else:
            logger.debug(f'Final value of mark_q worked: {mark_q}')
            control.update({**c1, **c2})
            break
    return control


def read_section11(th, mark_q):
    logger.info('Start reading section 11...')
    q_labels = ['Oxygen', 'Intubation ER', 'Intubated by Ambulance']
    breath = {i: 'NA' for i in q_labels}
    while True:
        try:
            rest = ReadMarks(th, sorted_cnts, 11)\
                .gray_blur_edge(contrast=0, bright=0, sig=0, q=mark_q, show_img=False)\
                .thresholding().find_contour('otsu', 'ccomp', return_self=True)\
                .filter_mark_contours(sample_dim=[503, 813], shape=MARK_SHAPE, ratio=MARK_RATIO,
                                      area=[430., 215, 300., 60.], debug=True, show_contour=False)\
                .read_box_answers(ncol=2, nrow=3, a_label={0: 1, 1: 0}, q_label=q_labels)
        except:
            q = mark_q[0]
            q -= 2
            mark_q = (q, q)
            logger.debug(f'Next lookup value of mark_q: {mark_q}')
            if q < 0:
                logger.debug(
                    f"Can't fine the rigt parameters. Values return to NA.")
                mark_q = reset_q()
                break
        else:
            logger.debug(f'Final value of mark_q worked: {mark_q}')
            breath.update(rest)
            break
    return breath


def merge_results(*sections):
    return {j: k for i in sections for j, k in i.items()}


def pipe_exe(imgurl):
    """
    execute all steps 
    """
    global reset_q, MARK_RATIO, MARK_SHAPE, sorted_cnts
    photo = import_image(imgurl)
    form = extract_form(photo)
    check_resolution(form)
    mark_q, sect_sig, reset_q = set_parameters(form)
    th = preprocess(form)
    sorted_cnts = find_section_contours(th, sect_sig)
    logger.info('Start reading image...')
    MARK_SHAPE = [90, 19, 90, 19]
    MARK_RATIO = [1.5, 0.6]

    rest5 = read_section5(th, mark_q)
    rest2 = read_section2(th, mark_q)
    rest7 = read_section7(th, mark_q)
    rest8 = read_section8(th, mark_q)
    rest9 = read_section9(th, mark_q)
    rest10 = read_section10(th, mark_q)
    rest11 = read_section11(th, mark_q)
    omr_result = merge_results(
        rest2, rest5, rest7, rest8, rest9, rest10, rest11)
    print(omr_result)


if __name__ == '__main__':
    pipe_exe('images/form_v6_11.jpg')
