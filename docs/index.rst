.. MRR-OMR documentation master file, created by
   sphinx-quickstart on Wed Dec  9 14:11:07 2020.

Welcome to MRR-OMR's documentation!
===================================


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
