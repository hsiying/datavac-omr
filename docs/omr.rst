omr package
===========

Submodules
----------

omr.contour\_ops module
-----------------------

.. automodule:: omr.contour_ops
   :members:
   :undoc-members:
   :show-inheritance:

omr.implot module
-----------------

.. automodule:: omr.implot
   :members:
   :undoc-members:
   :show-inheritance:

omr.improcess module
--------------------

.. automodule:: omr.improcess
   :members:
   :undoc-members:
   :show-inheritance:

omr.multiple\_choice module
---------------------------

.. automodule:: omr.multiple_choice
   :members:
   :undoc-members:
   :show-inheritance:

omr.read\_marks module
----------------------

.. automodule:: omr.read_marks
   :members:
   :undoc-members:
   :show-inheritance:

omr.read\_pic module
--------------------

.. automodule:: omr.read_pic
   :members:
   :undoc-members:
   :show-inheritance:

omr.utils module
----------------

.. automodule:: omr.utils
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: omr
   :members:
   :undoc-members:
   :show-inheritance:
