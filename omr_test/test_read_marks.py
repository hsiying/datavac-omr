"""Test read_marks module"""
from omr.read_marks import img_detect_answer, translate_answer, detect_answers


def test_img_detect_answer():
    """Test read_marks.img_detect_answer """
    ans = [0, 1, 2, 3, 4, 5, 6, 7, 8]
    expect = [2, 2, 2]
    actual = img_detect_answer(ans, 3, 3)
    assert expect == actual, f'expected {expect}, got {actual}.'


def test_translate_answer():
    """Test read_marks.translate_answer """
    ans = [2, 1, 0]
    expect = ['No', 'Yes', 'NA']
    actual = translate_answer(ans, {0: 'NA', 1: 'Yes', 2: 'No'}, reverse=False)
    expect2 = ['NA', 'Yes', 'No']
    actual2 = translate_answer(ans, {0: 'NA', 1: 'Yes', 2: 'No'})
    assert expect == actual and expect2 == actual2,\
        f'expected {expect}, got {actual} \
             \nor expected {expect2}, got {actual2}.'


def test_detect_answer():
    """Test read_marks.detect_answer """
    l1 = [1, 2, 3, 10]
    label = {0: 'a', 1: 'b', 2: 'c', 3: 'd'}
    expect = 'd'
    actual = detect_answers(l1, a_label=label)
    assert expect == actual, f'expected {expect}, got {actual}.'

    l2 = [10, 14, 12, 13]
    label = {0: 'a', 1: 'b', 2: 'c', 3: 'd'}
    expect = -999
    actual = detect_answers(l2, a_label=label)
    assert expect == actual, f'expected {expect}, got {actual}.'
