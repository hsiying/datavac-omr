"""Test read_pic module"""
import cv2
from omr.read_pic import low_resolution


def test_low_resolution():
    """Test read_pic.low_resolution """
    image = cv2.imread('images/form_v6_11.jpg')
    expect = False
    actual = low_resolution(image)
    assert expect == actual, f'expected {expect}, got {actual}.'
