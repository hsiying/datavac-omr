"""Test contour_ops module"""
# import sys
# sys.path.insert(1, '/Users/outboundbird/Work/DataVac/')
from omr.contour_ops import one_dim_cluster, within_range, within_ratio


def test_sort_centroid():
    """Test contour_ops. """
    pass


def test_match_contour():
    """Test contour_ops. """
    pass


def test_find_contour():
    """Test contour_ops. """
    pass


def test_filter_contour_layer():
    """Test contour_ops. """
    pass


def test_by_n_edge():
    """Test contour_ops. """
    pass


def test_by_area_size():
    """Test contour_ops. """
    pass


def test_filter_marks():
    """Test contour_ops. """
    pass


def test_one_dim_cluster():
    """Test contour_ops. """
    sample = [234, 236, 345, 789]
    expect = [0, 0, 1, 2]
    actual = one_dim_cluster(sample)
    assert actual == expect, f'expected {expect} got {actual}'


def test_within_range():
    """Test contour_ops. """
    x, y, area_range = [10, 20, [25, 5, 15, 5]]
    expect = True
    actual = within_range(y, x, area_range)
    assert expect == actual, f'expected {expect}, got {actual}'


def test_within_ratio():
    """Test contour_ops. """
    ar, ratio_range = [1.0, [1.2, 0.8]]
    expect = True
    actual = within_ratio(ar, ratio_range)
    assert expect == actual, f'expected {expect}, got {actual}.'
