"""Read the photo"""
import logging
from typing import Union
from imutils.perspective import four_point_transform
import numpy as np
import imutils
import cv2

from omr.implot import plot_contour
from omr.utils import ImageNotFound, ContourNotFoundError
from omr.contour_ops import FindContour


def low_resolution(img: np.ndarray, frac: float = 0.25) -> bool:
    """
    Check if the image has low resolution.

    Args:
        img (np.ndarray): image
        frac (float, optional): ratio related to reference image.
         Defaults to 0.25.

    Returns:
        bool: True if image resolution is low, False otherwise
    """
    if img.any():
        logging.info(f'The dimension of the cropped image: {img.shape}')
    h, w = img.shape[0], img.shape[1]
    return (h * w) < (1530 * 2459 * frac)


def img_boundry(thresh: np.ndarray,
                blur: np.ndarray,
                approx_val: float = 0.03,
                show_contour: bool = False) -> np.ndarray:
    """
    Find the form boundries from the image.

    Args:
        thresh (np.ndarray): binary img; black background, white edges
        blur (np.ndarray): blured image to export to the downstream processing
        approx_val (float, optional): approximate ratio to mutiply
            by the number of edges. Defaults to 0.03.
        show_contour (bool, optional): if True plot image with contours.
            Defaults to False.

    Raises:
        ContourNotFoundError: if could not find a closed contour.
        ImageNotFound: if failed to find a 4-edge object.

    Returns:
        np.ndarray: perpendicular perspective of cropped image
    """

    cnts, _ = cv2.findContours(
        thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    if not cnts:
        raise ContourNotFoundError(cnts, "Did not find any contours!")
    if show_contour:
        plot_contour(blur, cnts)
    doc_cnt = None
    # sort the contours from the biggest (paper surface) to smallest
    # then approximate the contour with closest shape
    # if the shape has 4 edges, found the paper and break
    if cnts:
        cnts = sorted(cnts, key=cv2.contourArea, reverse=True)
        for cnt in cnts:
            peri = cv2.arcLength(cnt, True)
            approx = cv2.approxPolyDP(cnt, approx_val * peri, True)

            if len(approx) == 4:
                doc_cnt = approx
                break
            # else:
        # under-indent to raise the err in the end per pylint
        raise ImageNotFound(
            "Cannot find the sheet in this picture.\
                    Please try to retake the picture.")
    return four_point_transform(blur, doc_cnt.reshape(4, 2))


def img_crop(img: Union[list, np.ndarray],
             cnts: list) -> Union[list, np.ndarray]:
    """
    Crop img from the section contours.

    Args:
        img (Union[list ,np.ndarray]): original img.
        cnts (list): section contours.

    Returns:
        Union[list ,np.ndarray]: [description]
    """

    try:
        x, y, w, h = cv2.boundingRect(cnts)
    except ImageNotFound as err:
        logging.error(f'Error when cropping image. \n{err}')
    return img[y: y+h, x: x+w]


def blur_image(img: Union[list, np.ndarray], threshold: int = 100) -> bool:
    """
    Check if image is blur or not.

    Args:
        img (Union[list, np.ndarray]): image.
        threshold (int, optional): bluriness threshold. Defaults to 100.

    Returns:
        bool: True if image is blur, False otherwise.
    """
    return cv2.Laplacian(img, cv2.CV_64F).var() < threshold


def detect_orientation(img: Union[list, np.ndarray])\
        -> Union[list, np.ndarray]:
    """
    Rotate image if not within the defined orientation.

    Args:
        img (Union[list, np.ndarray]): image

    Returns:
        Union[list, np.ndarray]: image
    """

    h, w = img.shape
    if h / w > 1.2:
        rotate = imutils.rotate_bound(img, 270)
        return rotate
    return img


class FindPaper(FindContour):
    """
    Finding the paper from the image. Children of FindContour class.

    Attributes:
        FindContour ([type]): [description]

    Raises:
        ImageNotFound: [description]

    Returns:
        [type]: [description]
    """

    def __init__(self, img):
        self.img = img
        self.paper = None
        super().__init__(img)

    def __repr__(self):
        return f'parent: {super().__repr__()}'

    def boundry(self, approx_val: float = 0.02):
        """
        Find the form boundries from the image.
        Need to process gray_blur_edge().thresholding().find_controur()
        before this step.

        Args:
            approx_val (float, optional): (0, 1) the percentage of
                approximation of the contour PloyDP. Defaults to 0.02.
            self.cnts (list): contours inherited from parent
                 class `FindContour`.

        Raises:
            ImageNotFound: [description]

        Returns:
            list: perpendicular perspective of cropped image
        """

        cnts = self.cnts

        doc_cnt = []
        if cnts:
            cnts = sorted(cnts, key=cv2.contourArea, reverse=True)
            for cnt in cnts:
                peri = cv2.arcLength(cnt, True)
                approx = cv2.approxPolyDP(cnt, approx_val * peri, True)

                if len(approx) == 4:
                    doc_cnt = approx
                    break
            if not doc_cnt.all():
                raise ImageNotFound(
                    "**Error: [FindPaper- boundry] Cannot find the targeted\
                        object in this picture. Please try to retake \
                            the picture.")
        try:
            self.paper = four_point_transform(self.img, doc_cnt.reshape(4, 2))
        except ImageNotFound as err:
            logging.exception(
                f'**Exception: <FindPaper- boundry> Error when\
                     transforming image perspective. \n{err}')
        return self


def inclusive_range(start: int, stop: int, step: int):
    """
    Inclusive range.

    Args:
        start (int): Starting position.
        stop (int): stop position.
        step (int): step unit.

    Returns:
        list: list of number
    """
    return range(start, (stop+1) if step >= 0 else (stop - 1), step)


def uneven_tone(img: Union[list, np.ndarray],
                div: int = 3,
                test_thresh: int = 55) -> bool:
    """
    Test if the interested area has uneven tones.
        For example, overexposed or underexposed subareas

    Args:
        img (Union[list, np.ndarray]): image.
        div (int, optional): number of division in horizontal and
             vertical direction. Defaults to 3.
        test_thresh (int, optional): treshold to decide the max
             allowed difference. Defaults to 55.

    Returns:
        bool: True - there's uneven tones, False- the photo is homogenous
    """
    if len(img.shape) < 3:
        h, w = img.shape
    else:
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        h, w = img.shape
        # raise ImportError("The image should be on the gray scale.")
    # divide in to sections
    step_h = int(h/div)
    step_w = int(w/div)
    # print(f'h:{h}, w: {w} \nstep_h: {step_h}, step_w:{step_w}')
    h_l = list(inclusive_range(0, h, step_h))
    w_l = list(inclusive_range(0, w, step_w))
    # print(f'h:{h}, w:{w}\nlists: {h_l}\n{w_l}\n{"+"*29}')
    quads = [img[h_l[i]: h_l[i+1], w_l[j]: w_l[j+1]].mean()
             for i in range(len(h_l)-1) for j in range(len(w_l) - 1)]
    tone_diff = max(quads) - min(quads)
    # print(f'section tone difference: {tone_diff}')
    return tone_diff > test_thresh
