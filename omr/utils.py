"""Utility functions"""
import logging


class Error(Exception):
    """
    Base exception in this module.
    """


class ContourNotFoundError(Error):
    """
    No contour is passed. Children of Error class.
    """

    def __init__(self, var, msg):
        self.var = var
        self.msg = f'**Error: Did not find contour {var}, {msg}'
        logging.error(self.msg)
        super().__init__()


class ExtraArgsError(Error):
    """
    None-defined arguments in kwargs. Children of Error class.
    """

    def __init__(self, arg, msg):
        self.arg = arg
        self.msg = f'**Error: Unused argument {self.arg}, {msg}'
        logging.error(self.msg)
        super().__init__()


class ImageNotFound(Error):
    """
    Can not find image. Children of Error class.
    """

    def __init__(self, msg):
        self.msg = f'**Error: No image data has been passed the object, {msg}'
        logging.error(self.msg)
        super().__init__()


class MarkReadingError(Error):
    """
    Cannot read marks. Children of Error class.
    """

    def __init__(self, msg):
        self.msg = f'**Error: Mark read error- {msg}'
        logging.error(self.msg)
        super().__init__()


class ImageQualityError(Error):
    """
    Bad image quality. Children of Error class.
    """

    def __init__(self, msg):
        self.msg = f'**Error: Image quality error- {msg}'
        logging.error(self.msg)
        super().__init__()
