"""Contour operations."""
import logging
from typing import Tuple, Union
import numpy as np
import cv2
from omr.improcess import ImgPrep
from omr.utils import ExtraArgsError


def one_dim_cluster(list_: np.array, tol: int = 2) -> list:
    """
    Clustering on 1-d array/list.

    Args:
        list_ (np.array): list or np.array
        tol (int, optional): Tolerance. Defaults to 2.

    Raises:
        ValueError: If length of grouped list is not equal
        to the length of original list.

    Returns:
        list: List of cluster groups
    """
    j = 0
    grp = 0
    grp_l = []
    while j < len(list_):
        mean_ = list_[j]
        for i in range(j, len(list_)):
            j = i + 1
            mean = np.mean(list_[i: j+1])
            dist = abs(mean_ - mean)
            # print(f'i: {i}, j: {j}, dist: {dist}')
            if dist > tol:
                grp_l.append(grp)
                grp += 1
                # print(f'stop at j: {j}\ngroup:{grp}')
                break
            # else:  # pylint: disable=R1723
            grp_l.append(grp)
    if len(grp_l) != len(list_):
        raise ValueError(
            f"**Error: <oneD_cluster>: The length of the input list ({len(list_)}) \
                is not equal to the length of out list ({len(grp_l)}).")
    return grp_l


class SortContour():
    """
    Sort contours
    """

    def __init__(self, cnts):

        self.cnts = cnts
        self.d = None
        # self.method = method
        centers = []
        for cnt in cnts:
            M = cv2.moments(cnt)
            c_x = int(M['m10']/M['m00'])
            c_y = int(M['m01']/M['m00'])
            # print(f'centroid of the contour: {cx}, {cy}')
            centers.append((c_x, c_y))
        self.centers = centers

    def __repr__(self):
        return f'class: {self.__class__.__name__}\
            number of centers: {len(self.centers)}'

    def sort_centroid(self,
                      axis: int = 1,
                      reverse: bool = False,
                      tol: int = 2):
        """
        Wrapper of __sort_centroid function

        Args:
            axis (int, optional): x:0, y:1. Defaults to 1.
            reverse (bool, optional): if sort by descending order.
                                        Defaults to False.
            tol (int, optional): tolerance of the gap. Defaults to 2.

        Returns:
            nd.np.array: self
        """
        self.d = self.__sort_centroid(
            self.centers, axis=axis, reverse=reverse, tol=tol)
        return self

    @staticmethod
    def __sort_centroid(cntr, axis: int = 1, reverse: bool = False, tol=2):
        """
        Sort contours by centroid.

        Args:
            cntr (2d list/np.array): centroid coordinates [x, y]
            axis (int, optional): 0- x, 1-y coordinates. Defaults to 1.
            reversed (bool, optional): if True, sort from  right to  left,
                    buttom to top; if False, sort form left to right,
                    top to buttom. Defaults to False.
            tol (int, optional): Tolerance of the minimal mean of distance
                    increments between clusters. Defaults to 2.

        Raises:
            ValueError: if axis value other than 0, 1.
            ValueError: if lengths of contour centers and sorted centers are
                    different.

        Returns:
            [type]: [description]
        """

        if axis > 1 or axis < 0:
            raise ValueError(
                f"**Erorr: <SortContour- sort_centroid> \
                axis argument value should be either 0 or 1. Received {axis}")

        idx = 1 - axis
        cntr = sorted(cntr, key=lambda x: x[axis])
        list_ = np.array(cntr)[:, axis]
        grp = one_dim_cluster(list_, tol=tol)
        mtx = [[*i, j] for i, j in zip(cntr, grp)]
        mtx.sort(key=lambda u: (u[2], u[idx]), reverse=reverse)
        ordered = list(range(0, len(list_)))
        mtx = [[*mtx[i], ordered[i]] for i in range(len(mtx))]
        # self.dict_ = dict(((i[0], i[1]), i[3])for i in mtx)
        dict_ = dict(((i[0], i[1]), i[3])for i in mtx)
        if len(dict_) != len(list_):
            raise ValueError(
                f"The lengths are different between array of contour\
                     centers ({len(list_)}) \
                         and the sorted centers ({len(dict_)}).")
        return dict_

    def match_contour(self) -> list:
        """
        Wrapper for __match_countour function.

        Returns:
            list: counters
        """
        cnts = self.__match_contour(self.d, self.centers, self.cnts)
        return cnts

    @staticmethod
    def __match_contour(d: list, centr: list, cnts: list) -> list:
        """
        Match contours

        Args:
            d (list): sorted list
            centr (list): centroid
            cnts (list): contours before sorting,
                should be consistent with  centr

        Raises:
            ValueError: if the lengths of three arguments are different.

        Returns:
            list: a list of contours
        """
        if not len(d) == len(centr) == len(cnts):
            raise ValueError(f"**Error: <SortContour - match_contour>\
                 The lengths are different among contours ({len(cnts)}),\
                 contour centers ({len(centr)}), sorted contours({len(d)}).\
                     Check previous sorting step.")
        z = list(zip(centr, cnts))
        z.sort(key=lambda x: d[x[0]])
        return [i[1] for i in z]


def find_contour_(func) -> Tuple[list, list]:
    """
    Find contours based on chosen threshoding img option.
    decorator function.

    Args:
        func (find contour method): default otsu,

    Returns:
        Tuple[list, list]: cnts, hier
    """

    def contouring(self, thresh_opt, method):
        img4cnt = func(self, thresh_opt)
        return {'ccomp': cv2.findContours(
            img4cnt, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE),
            'external': cv2.findContours(
            img4cnt, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        }.get(method, None)
    return contouring


class FindContour(ImgPrep):
    """
    Find contours. Chindren of ImgPrep.
    """

    def __init__(self, img):
        self.img = img
        self.cnts = None
        self.hier = None
        super().__init__(img)

    def __repr__(self):
        return f'class: {super().__repr__()}'

    @find_contour_
    def _image_contour(self, thresh_opt: str):
        """
        Call find countour method.

        Args:
            thresh_opt (str): name of the methods
            options are: 'edge', 'otsu', 'adaptive'

        Returns:
            method: the method to find contour.
        """
        return {'edge': self.edge,
                'otsu': self.thresh_otsu,
                'adaptive': self.thresh_adpt
                }.get(thresh_opt, None)

    def find_contour(self, thresh_opt: str, method: str, return_self=False):
        """
        Find contours from image.

        Args:
            thresh_opt (str): thresholding method.
            method (str): method for finding contours.
            return_self (bool, optional): if return to class object.
                Defaults to False.

        Returns:
            [type]: [description]
        """

        self.cnts, self.hier = self._image_contour(thresh_opt, method)
        if return_self:
            return self
        return self.cnts, self.hier


class FilterContour:
    """
    Filter contours.

    Attributes:
        cnts (list): list of contours
        hier (list): list of contour hierarchy
    """

    def __init__(self,
                 cnts: Union[list, np.ndarray],
                 hier: Union[list, np.ndarray]):
        """
        Filter contours by different standards.

        Args:
            cnts (Union[list, np.ndarray]): Contours.
            hier (Union[list, np.ndarray]): Contour hierarchy.
        """
        self.cnts = cnts
        self.hier = hier

    def __repr__(self):
        return f'class: {self.__class__.__name__}'

    def filter_layer(self, layer: int = 2, **params) -> Tuple[list, list]:
        """
        Filter out the contours by layer.

        Args:
            layer (int, optional): layer index: 2, external, 3 internal.
            Defaults to 2.
            **params (str): 'cnts' for contours, 'hier' for contour hierarchy.

        Raises:
            ExtraArgsError: if provided argument name not in **params options

        Returns:
            Tuple[list, list]: Filtered contours and contour hierarchy
        """

        cnts, hier = params.get(
            'cnts', self.cnts), params.get('hier', self.hier)
        # or warning??
        parm_names = ['cnts', 'hier']
        extra_arg = set(params.keys()).difference(set(parm_names))
        if extra_arg:
            raise ExtraArgsError(
                extra_arg, f'**Error: {extra_arg} not definded in the \
                    <FilterContour- filter_layer> function arguments set.')

        if isinstance(hier, list):
            hier = np.array(hier)

        ext_cont = []
        ext_hier = []
        if len(hier.shape) > 2:
            hier = hier[0]
        for cont, hier_ in zip(cnts, hier):
            # print(f'hier: {hier_[2]}\ncnts: {cont}')
            if hier_[layer] == -1:
                ext_cont.append(cont)
                ext_hier.append(hier_)
        return ext_cont, ext_hier

    def by_n_edge(self,
                  frac: float = 0.02,
                  n_edge: int = 4,
                  **params) -> Tuple[list, list]:
        """
        Filter contours by number of edges.

        Args:
            frac (float, optional): percentage round up of found edges.
                                    Defaults to 0.02.
            n_edge (int, optional): number of edges. Defaults to 4.
            params (str): 'cnts' for contours, 'hier' for contour hierarchy.

        Raises:
            ExtraArgsError: if provided argument name not in **params options.

        Returns:
            Tuple[list, list]: filtered contours and hierarchy
        """
        cnts, hier = params.get(
            'cnts', self.cnts), params.get('hier', self.hier)

        parm_names = ['cnts', 'hier']
        extra_arg = set(params.keys()).difference(set(parm_names))
        if extra_arg:
            raise ExtraArgsError(
                extra_arg, 'not definded in the <FilterContour- by_n_edge> \
                    function arguments set.')

        squared_cnts = []
        squared_hier = []

        if isinstance(hier, list):
            hier = np.array(hier)

        if len(hier.shape) > 2:
            hier = hier[0]

        for cnt, hier in zip(cnts, hier):
            peri = cv2.arcLength(cnt, True)
            approx = cv2.approxPolyDP(cnt, frac * peri, True)
            if len(approx) == n_edge:
                squared_cnts.append(cnt)
                squared_hier.append(hier)

        return squared_cnts, squared_hier

    def by_area_size(self,
                     min_a: Union[float, int],
                     max_a: Union[float, int],
                     scale: float = 1,
                     verbose: bool = False,
                     **params) -> list:
        """
        Filter by contour area size.

        Args:
            min_a (float or int): minimum area in pixels
            max_a (float or int): maximum area in pixels
            scale (float, optional): scaling factor. Defaults to 1.
            verbose (bool, optional): if print log. Defaults to False.
            params (str): 'cnts' for contours.

        Raises:
            ExtraArgsError: if provided argument name not in **params options.

        Returns:
            list: filetered contours.
        """

        cnts = params.get('cnts', self.cnts)
        parm_names = ['cnts']
        extra_arg = set(params.keys()).difference(set(parm_names))
        if extra_arg:
            raise ExtraArgsError(
                extra_arg, 'not definded in the <FilterContour- by_area_size> \
                    function arguments set.')

        max_a = max_a * scale
        min_a = min_a * scale
        out_cnts = list(
            filter(lambda x: max_a > cv2.contourArea(x) > min_a, cnts))

        if verbose:
            logging.debug('found %s block contours', len(out_cnts))
        return out_cnts


def within_range(y: int, x: int, area_range: list) -> bool:
    """
    Test is the range is in the desinated contour range.

    Args:
        y (int): image range on y axis.
        x (int): image range on x axis.
        area_range (list): four-element ymin, ymax, xmin, xmax list or array.

    Returns:
        bool: True if within the range, Fale otherwise.
    """
    return (area_range[0] > y > area_range[1]
            and area_range[2] > x > area_range[3])


def within_ratio(aspect_ratio: float, ratio_range: list) -> bool:
    """
    Check if contour is with within aspect ratio.

    Args:
        aspect_ratio (float): aspect ratio
        ratio_range (list): two-element list or tuple

    Returns:
        bool: [description]
    """
    return ratio_range[0] > aspect_ratio > ratio_range[1]


def img_filter_marks(cnts: list,
                     scale: float,
                     area_range: list,
                     shape_range: list,
                     ratio_range: list,
                     verbose=False) -> list:
    """
    Filter in the contours within the image area, shape, ar ratio.

    Args:
        cnts (list): contours
        scale (float): scaler compared to training image
        area_range (list): area range
        shape_range (list): shape range
        ratio_range (list): aspect ratio range
        verbose (bool, optional): if print the process. Defaults to False.

    Returns:
        list: list of contours
    """
    area_range = list(map(lambda x: x*scale, area_range))
    shape_range = list(map(lambda x: x*scale, shape_range))

    box_cnts = []
    for cnt in cnts:
        x, y, w, h = cv2.boundingRect(cnt)
        a_r = float(w)/h
        # logging.info(f'x:{x}, y:{y}, w:{w}, h:{h}, ar:{ar}')
        if within_range(y, x, area_range) \
                and within_range(h, w, shape_range)\
                and within_ratio(a_r, ratio_range):
            box_cnts.append(cnt)
    if verbose:
        logging.debug(  # pylint: disable=logging-fstring-interpolation
            f"Number of marker contours detected: {len(box_cnts)}")
    return box_cnts
