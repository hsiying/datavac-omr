"""Image processing."""
import logging
import numpy as np
import cv2

from omr.implot import plot_image
from omr.utils import ImageNotFound, ExtraArgsError


def contrast_bright_adj(img: np.array,
                        contrast: int,
                        brightness: int) -> np.array:
    """
    Adjust for contrast and brightness of image.

    Args:
        img (np.array): image
        contrast (int): contract number
        brightness (int): brightness number

    Raises:
        ImageNotFound: [description]

    Returns:
        np.ndarray: image after adjustment
    """
    if isinstance(img, list):
        img = np.array(img)
    if not img.tolist():
        raise ImageNotFound('in <contrast_bright_adj> function.')
    img = np.int16(img)
    img = img * (contrast/127+1) - contrast + brightness
    img = np.clip(img, 0, 255)
    img = np.uint8(img)
    return img


def img_scale(img: np.ndarray, sample_dim: list) -> float:
    """
    Calculate scale of dim between sample img and cropped image.

    Args:
        img (np.ndarray): cropped img.
        sample_dim (list): the dimention of the refrence image

    Raises:
        ImageNotFound: if np.array cannot convert to list.

    Returns:
        float: scale factor between uploaded image and reference image.
    """
    if isinstance(img, list):
        img = np.array(img)
    if not img.tolist():
        raise ImageNotFound('in <img_scale> function.')

    img_dim = img.shape
    try:
        return 1/((sample_dim[0]/img_dim[0] + sample_dim[1]/img_dim[1])/2)
    except ImageNotFound as err:
        logging.exception(f'**Error: <img_scale>, {err}')


class ImgPrep:
    """
    The class for image preprocessing for recognition procedure.
    """

    def __init__(self, img: np.ndarray):
        """
        Args:
            img (np.ndarray): image

        Raises:
            ImageNotFound: if it's empty object
        """
        if isinstance(img, list):
            img = np.array(img)
        if not img.tolist():
            raise ImageNotFound('in <ImgPrep> class.')
        self.img = img
        self.gray = None
        self.contrast = None
        self.blur = None
        self.edge = None
        self.thresh_otsu = None
        self.thresh_adpt = None
        self.resized = None

    def gray_blur_edge(self, show_img: bool = False, **params):
        """
        Pretreatment of the image for gray, blur, edge images.

        Args:
            show_img (bool, optional): if True show image. Defaults to False.
            params:
                contrast: int default 0 (enhance contrast < 0)
                bright: int, default 0 (darken < 0)
                q: int kernel unit
                sig: noise deduction level
                thresh_l: lower boundry of threshold, default 0
                thresh_h: higher boundry of threshold, default 255

        Raises:
            ExtraArgsError: if misspecify the params

        Returns:
            np.ndarray: image after treatment
        """

        q = params.get('q', (7, 7))
        sig = params.get('sig', 3)
        contrast = params.get('contrast', 0)
        bright = params.get('bright', 0)
        canny_l = params.get('canny_l', 0)
        canny_h = params.get('canny_h', 255)

        parm_names = ['q', 'sig', 'contrast', 'bright', 'canny_l', 'canny_h']
        extra_arg = set(params.keys()).difference(set(parm_names))
        if extra_arg:
            raise ExtraArgsError(
                extra_arg, 'not defined in the <ImgPrep- gray_blur_edge>\
                 function arguments set.')

        if len(self.img.shape) > 2:
            self.gray = cv2.cvtColor(self.img, cv2.COLOR_BGR2GRAY)
        else:
            self.gray = self.img

        self.contrast = contrast_bright_adj(
            self.gray, contrast=contrast, brightness=bright)
        self.blur = cv2.GaussianBlur(self.contrast, q, sig)
        self.edge = cv2.Canny(self.blur, canny_l, canny_h)
        if show_img:
            plot_image(self.blur, 'Blurred')
        return self

    def thresholding(self, **params):
        """
        Apply thresholding to the image.

        Args:
            params:
                thresh_l: the lower bound of thresholding value
                thresh_h: the upper bound of thresholding value
                blocksize: blocksize used in the adaptive thresholding method
                c: c use in the adaptive thresholding method

        Raises:
            ExtraArgsError: [description]

        Returns:
            np.ndarray: self to two thresholding methods otsu and \
                gaussian adaptive thresholding
        """

        thresh_l = params.get('thresh_l', 0)
        thresh_h = params.get('thresh_h', 255)
        blocksize = params.get('blocksize', 9)
        c = params.get('c', 5)

        parm_names = ['thresh_l', 'thresh_h', 'blocksize', 'c']
        extra_arg = set(params.keys()).difference(set(parm_names))
        if extra_arg:
            raise ExtraArgsError(
                extra_arg, 'not definded in the <ImgPrep- thresholding>\
                     function arguments set.')

        _, self.thresh_otsu = cv2.threshold(
            self.blur, thresh_l, thresh_h, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        self.thresh_adpt = cv2.adaptiveThreshold(
            self.blur, 200, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,
            blocksize, c)
        return self


class ResizeImg(ImgPrep):
    """
    Resize image, children of ImgPrep.
    Attributes:
        image (np.ndarray): The image.

    """

    def __init__(self, img: np.ndarray):
        """
        Initiate the class

        Args:
            img (np.ndarray): image
        """

        self.img = img
        super().__init__(img)
        self.h, self.w, self.cc = img.shape
        self._ref_dim = [32, 128, self.cc]
        self.bgcolor = (255, 255, 255)
        self.scales = []
        # self.thresh_otsu = []
        # need to binarize image first

    @property
    def ref_dim(self):
        """
        Reference dimension [h, w, cc].
        """
        return self._ref_dim

    @ref_dim.setter
    def ref_dim(self, newlist: list):
        """
        Reassign reference dimension.

        Args:
            newlist (list): [h, w, cc]
        """
        # check all new assigned values are int
        self._ref_dim = newlist

    def resize_pad_image(self):
        """
        Resize and pad image.

        Returns:
            np.ndarray: Resized and padded image.
        """
        try:
            self.thresh_otsu
        except ValueError as err:
            logging.error(f'You need to thresholding the image {err}.')

        self.scales = [self.ref_dim[0]/self.h, self.ref_dim[1]/self.w]
        # print(f'self.scales {self.scales}')
        idx = np.argmin(self.scales)
        g, q = int(self.h * self.scales[idx]), int(self.w * self.scales[idx])
        e_h, e_w = int(0.5 * (self.ref_dim[0] - g)
                       ), int(0.5 * (self.ref_dim[1] - q))
        # print(f'g: {g}, q:{q}, eh:{eh}, ew:{ew}')

        self.bgcolor = 255
        canvas = np.full(self.ref_dim, self.bgcolor, dtype=np.uint8)
        # print(f'shape of self.img {self.img.shape}')
        resize = cv2.resize(self.thresh_otsu, (q, g),
                            interpolation=cv2.INTER_CUBIC)
        # print(f'shape of resize {resize.shape}')
        # plot_image(resize)
        # print(resize)
        canvas[e_h: e_h+g, e_w: e_w+q] = resize
        self.resized = canvas
        return self

# canvas = ResizeImg(image).gray_blur_edge().thresholding()
# canvas.ref_dim = [32, 128]
# canvas.resize_pad_image()
# plot_image(canvas.resized)
