"""MRR-OMR"""
import logging
logging.getLogger(__name__).addHandler(logging.NullHandler())
# The __name__ global in a module is the module name,
#  including parent packages.
# logging creates a hierarchy of logging objects based on . delimiters,
# so a logger in foo.bar propagates up to foo;
# by using logger = logging.getLogger(__name__)
# in each module in your library you make it easier
#  on yourself in that respect.

__version__ = '0.1.4'
