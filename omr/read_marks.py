""" Reading marks from boxes"""
import logging
from typing import Any, Union
from imutils import contours
import numpy as np
import cv2
# from matplotlib.patches import Rectangle

from omr.contour_ops import SortContour
from omr.implot import plot_bounding_box_order
from omr.utils import MarkReadingError


class ReadContourPxSize:
    """
    Reading contour by pixel size.
    """

    def __init__(self,
                 img: np.ndarray,
                 mark_cnts: list,
                 ncol: int,
                 verbose: bool = False):
        """
        Reading contour by pixel size.

        Args:
            img (np.ndarray): image treated with threshold
            mark_cnts (list): contours for marks
            ncol (int): number of columns in the mark contours
            verbose (bool, optional): [description]. Defaults to False.
        """
        self.img = img
        self.mark_cnts = mark_cnts
        self.ncol = ncol
        self.verbose = verbose

    def __repr__(self):
        return f'class: {self.__class__.__name__} \
                image dim: {np.array(self.img).shape}\
                length of contours: {len(self.mark_cnts)} ncol: {self.ncol}'

    def img_read_answer(self):
        """
        Read answers by contour pixel size.

        Raises:
            MarkReadingError: If the remainder of number of contours
             to number of columns is not 0.

        Returns:
            list: position of marked answers
        """

        marked = []
        for _, i in enumerate(np.arange(0, len(self.mark_cnts), self.ncol)):
            cnts = contours.sort_contours(self.mark_cnts[i: i+self.ncol])[0]
            for _, c in enumerate(cnts):
                mask = np.zeros(self.img.shape, dtype='uint8')
                cv2.drawContours(mask, [c], -1, (1, 255, 0), 1)
                mask = cv2.bitwise_and(self.img, self.img, mask=mask)
                total = cv2.countNonZero(mask)
                marked.append(total)
                if self.verbose:
                    print(f"Total count of the contour pixels: {total}.")
        if self.verbose:
            logging.debug(f"Total number of mark contours read: {len(marked)}")
        if len(marked) % self.ncol != 0:
            raise MarkReadingError(
                'The contours read is inconsistent with number of\
                 markers in the image.')
        return marked


class ReadBoundingBoxSize(SortContour):
    """
    Read marks by the size of bounding boxes, children of SortContour.

    Attributes:
            cnts (list): [description]
            ncol (int): [description]

    Raises:
        MarkReadingError: [description]

    Returns:
        [type]: [description]
    """

    def __init__(self, cnts: list, ncol: int):
        """
        Reading marks by bounding box sizes.

        Args:
            cnts (list): [description]
            ncol (int): [description]
        """
        self.cnts = cnts
        self.ncol = int(ncol)
        super().__init__(cnts)

    def __repr__(self):
        return f'class: {super().__repr__()}\
               ,contour length: {len(self.cnts)} \nncol: {self.ncol}'

    def read_box_size(self, tol: int = 4, show_box: bool = False, **params):
        """
        Read bounding box sizes.

        Args:
            tol (int, optional): tolerance. Defaults to 4.
            show_box (bool, optional): show plot. Defaults to False.

        Raises:
            MarkReadingError: if ncol is inconsistent with number of marks.

        Returns:
            list: bounding box coordinates
        """
        def bonding_box_area(cnt: list) -> int:
            """
            Calculate bonding box area

            Args:
                cnt (list): list of contours

            Returns:
                int: Area of bonding boxes
            """
            *_, w, h = cv2.boundingRect(cnt)
            return w*h

        sorted_cnts = super().sort_centroid(tol=tol).match_contour()
        if show_box:
            # if show box, furnish 'img' argument otherwise get an error
            img = params.get('img', "Make sure get the correct image object!")
            try:
                plot_bounding_box_order(img, sorted_cnts)
            except MarkReadingError as err:
                logging.error(f'**Error: <ReadBoundingBoxSize - \
                ploting_bounding_box> cannot plot bounding box order: {err}')

        boxes = list(map(bonding_box_area, sorted_cnts))
        if len(boxes) % self.ncol != 0:
            raise MarkReadingError(f'**Error: <ReadBoundingBoxSize - \
            read_box_size> The lengths contours read ({len(boxes)})is \
            inconsistent with number of markers in the image.')
        return boxes


class ReadIntensity(SortContour):
    """
    Read marks by intensity, children of SortContour.

    Attributes:
        img (np.ndarray): image
        cnts (list): list of contours
        ncol (int): number of columns
    """

    def __init__(self, img: np.ndarray, cnts: list, ncol: int):
        """
        Read marks by intensity.

        Args:
            img (np.ndarray): image
            cnts (list): list of contours
            ncol (int): number of columns
        """
        self.cnts = cnts
        self.img = img
        self.ncol = ncol
        self.sorted_cnts = None
        super().__init__(self.cnts)

    def __repr__(self):
        return f'class: {super().__repr__()}\
                \ncontour length: {len(self.cnts)} \nncol: {self.ncol}'

    @staticmethod
    def boundingbox_intensity(cnt: list, img: np.ndarray) -> float:
        """
        Calculate bounding box intensity.

        Args:
            cnt (list): list of contours.
            img (np.ndarray): image.

        Returns:
            float: averaged intensity inside bounding boxes.
        """
        x, y, w, h = cv2.boundingRect(cnt)
        return np.mean(img[y: y+h, x: x+w])

    def read_intensity(self, tol: int) -> float:
        """
        Read intensity of bounding boxes

        Args:
            tol (int): tolerance of sorting centroid

        Raises:
            MarkReadingError: number of marks and number of columns
            are incosistent

        Returns:
            float: list of intensity values
        """
        self.sorted_cnts = super().sort_centroid(tol=tol).match_contour()
        intensity = list(
            map(lambda x: self.boundingbox_intensity(x, self.img),
                self.sorted_cnts))
        if len(intensity) % self.ncol != 0:
            raise MarkReadingError(f'**Error: <ReadBoundingBoxSize - \
            read_box_size> The lengths contours read ({len(intensity)})\
            is inconsistent with number of markers in the image.')
        return intensity


def img_detect_answer(answers: list, ncol: int, nrow: int) -> list:
    """
    Detecting answers from the list.
    returned from img_read_answer function np.array

    Args:
        answers (list): list of answers.
        ncol (int): number of columns.
        nrow (int): number of rows.

    Raises:
        ValueError: if total number of answers is not equal to product
         of number of columns and number of rows.

    Returns:
        list: detected answer list
    """
    if len(answers) != nrow * ncol:
        raise ValueError(
            f'**Error: <img_detect_answer> incompatible list lenth\
             ({len(answers)}) and matrix shape ({nrow}, {ncol}).')

    answer_mtx = np.reshape(answers, (nrow, ncol))
    return answer_mtx.argmax(axis=1).tolist()


def translate_answer(answers: list, labels: dict, reverse=True) -> list:
    """
    Translate image read answers to the verbal text.

    Args:
        answers (list): list of answer positions.
            1-d list returned from img_detect_answer.
        labels (dict): dict labels of the corresponding answer texts.
        reverse (bool, optional): if read answers from bottom up.
            Defaults to True.

    Returns:
        list: list of text answers
    """
    if reverse:
        return [labels[i] for i in answers[:: -1]]
    return [labels[i] for i in answers]


def detect_answers(mark_param: Union[list, np.array],
                   a_label: dict,
                   tol: float = 1.25,
                   NA: Union[Any] = -999) -> dict:
    """
    Detect checked marks by comparing returned mark image parameters.
        Have the option of non-checked return to -999. If the parameters are
        approximately the same. It regards as non-checked.

    Args:
        mark_param (Union[list, np.array]): parameters, can be contour length,
                         boundingbox area, area intensity
        a_label (dict): answer choice labels in dict format, e.g. 0- no; 1 -yes
        tol (float, optional): tolerance for ratio to be recognized as equal
                 (approx = 1), default 1.25. varies by parameter.
                 Defaults to 1.25.
        NA (Union[Any], optional): NA symbols. Defaults to -999.

    Returns:
        dict: labeled answers
    """

    # comparing the ratio between the max value of param and each of the param
    # if the it's approximately equals to 1, none of them are checked.
    # if it's higher than the threshold, it regards as the max value
    # mark is checked.

    max_pos = np.argmax(mark_param)
    le_max = mark_param[max_pos]

    # the_max = mark_param[0]
    mark_ratio = list(map(lambda x: le_max/x, mark_param))
    # 0, not chosen 1, checked
    answers = list(map(lambda x: 1 if x < tol else 0, mark_ratio))
    # print(f'sum of answers: {sum(answers)}')
    # checked_pos
    if sum(answers) > 1:
        logging.warning('Unable to detect answers, return missing value.')
        checked_pos = None
    else:
        checked_pos = np.argmax(answers)
        # print(f'checked position: {checked_pos}')

    if checked_pos is not None:
        return a_label[checked_pos]
    return NA
