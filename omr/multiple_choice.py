"""Read multiple choice sections."""
import logging
from typing import Union
import numpy as np

import omr.read_marks as rm
import omr.read_pic as rp
from omr.utils import ExtraArgsError, MarkReadingError
from omr.improcess import img_scale
from omr.contour_ops import FilterContour, img_filter_marks, FindContour
from omr.read_marks import (ReadContourPxSize, ReadBoundingBoxSize,
                            ReadIntensity, detect_answers)
from omr.implot import (plot_image, plot_contour, plot_bounding_box)


class ReadMarks(FindContour):
    """
    Pipeline for reading marking contours. Children of FindContour class.
    """

    def __init__(self, img: Union[list, np.ndarray],
                 cnts: list,
                 cnt_n: int,
                 show_img: bool = False):
        """
        Reading marks.

        Args:
            img (Union[list, np.ndarray]): the image/ form after extracting
                 the form out of the photo.
            cnts (list): contours.
            cnt_n (int): contour number, the rank from the sorted contours.
            show_img (bool, optional): diagnostic procedure, show image.
                 Defaults to False.
        """
        self.img = img
        self.cnts = cnts
        self.cnt_n = cnt_n
        self.crop_img = rp.img_crop(self.img, self.cnts[self.cnt_n])
        self.ext_cnts, self.ext_hier, self.scale = None, None, None
        self.answers, self.box_cnts = None, None
        super().__init__(self.crop_img)
        if show_img:
            plot_image(self.crop_img, 'Cropped section')

    def __repr__(self):
        return f'class: {self.__class__.__name__}'

    def filter_mark_contours(self, sample_dim: list,
                             debug: bool = False,
                             show_contour: bool = False,
                             **params):
        """
        Filter marking contours.

        Args:
            sample_dim (list): dimention from the reference picture.
            debug (bool, optional): debug. Defaults to False.
            show_contour (bool, optional): plot contours. Defaults to False.
            params:
                shape: the x, w, y, h dim of the mark contour.
                ratio: w/h ratio.
                area: the area coord contains the mark contour.

        Raises:
            ExtraArgsError: [description]

        Returns:
            [type]: [description]
        """

        # filter external layer
        self.ext_cnts, self.ext_hier = FilterContour(
            self.cnts, self.hier).filter_layer()
        if show_contour:
            plot_contour(self.thresh_otsu, self.ext_cnts)
            plot_bounding_box(self.blur, self.ext_cnts)

        # scale up or down the image accroding to the ref image
        self.scale = img_scale(self.crop_img, sample_dim=sample_dim)
        if debug:
            logging.debug(f'scale = {self.scale :.2f}')
            logging.debug(
                f'original_contour: {len(self.cnts)},\
                 filtered exterior contour: {len(self.ext_cnts)}')
            print(f'original_contour: {len(self.cnts)}, \
                filtered exterior contour: {len(self.ext_cnts)}')

        shape_range = params.get('shape', [60., 15., 40., 15.])
        ratio_range = params.get('ratio', [1.5, 0.67])
        area_range = params.get('area', [670., 160, 670., 235.])

        parm_names = ['shape', 'ratio', 'area']
        extra_arg = set(params.keys()).difference(set(parm_names))
        if extra_arg:
            raise ExtraArgsError(
                extra_arg, 'not definded in the\
                     <filter_mark_contours> function arguments set.')

        self.box_cnts = img_filter_marks(
            self.ext_cnts, self.scale, area_range,
            shape_range, ratio_range, verbose=debug)
        if show_contour:
            print(f' number of boxCnts: {len(self.box_cnts)}')
            plot_contour(self.blur, self.box_cnts, title='mark contours')
            plot_bounding_box(self.blur, self.box_cnts)
        return self

    def read_px_answers(self, ncol: int,
                        nrow: int,
                        a_label: dict,
                        q_label: dict,
                        verbose=False):
        """
        Read the selected answers by comparing contour length.

        Args:
            ncol (int): number of columns of the marks.
            nrow (int): number of rows of the marks.
            a_label (dict): label of answers.
            q_label (dict): label of questions.
            verbose (bool, optional): [description]. Defaults to False.

        Returns:
            [type]: [description]
        """

        answers = ReadContourPxSize(
            self.blur, self.box_cnts, ncol, verbose=verbose).img_read_answer()
        selected = rm.img_detect_answer(answers, ncol, nrow)
        rest = rm.translate_answer(selected, a_label)
        return {q_label[i]: rest[i] for i in range(len(rest))}

    def read_box_answers(self, ncol: int,
                         nrow: int,
                         a_label: dict,
                         q_label: dict,
                         debug=False,
                         tol: int = 6,
                         tol_ra: int = 1.25) -> dict:
        """
        Read the selected answers by comparing the size of bounding boxes.

        Args:
            ncol (int): number of columns.
            nrow (int): number of rows.
            a_label (dict): answer labels.
            q_label (dict): question labels.
            debug (bool, optional): should print debug messages?
                 Defaults to False.
            tol (int, optional): tolerance for read box size. Defaults to 6.
            tol_ra (int, optional): tolerance for reading answers (ratio).
                 Defaults to 1.25.

        Returns:
            dict: detected answers tranlated to text.
        """

        try:
            self.answers = ReadBoundingBoxSize(self.box_cnts, ncol).\
                read_box_size(tol=tol,
                              show_box=debug,
                              img=self.crop_img)
        except MarkReadingError as err:
            logging.error(
                f'**Error: <ReadBoundingBoxSize - read_box_answer> :\
                     the read answers: {self.answers}. {err}')
        answer_mtx = np.array(self.answers).reshape((nrow, ncol))
        if debug:
            print(answer_mtx)

        selected = [detect_answers(i, tol=tol_ra, a_label=a_label)
                    for i in answer_mtx]
        if debug:
            print(selected)
        return {q_label[i]: selected[i] for i in range(len(selected))}

    def read_intensity_answers(self, ncol: int,
                               nrow: int,
                               a_label: dict,
                               q_label: dict,
                               tol: int = 6):
        """
        Read answers by intensity.

        Args:
            ncol (int): number of columns.
            nrow (int): number of rows.
            a_label (dict): answer labels.
            q_label (dict): question labels.
            tol (int, optional): tolerance for read box size. Defaults to 6.

        Returns:
            dict: dected answers translated to text.
        """
        try:
            answers = ReadIntensity(
                self.thresh_otsu, self.box_cnts, ncol).read_intensity(tol=tol)
            print(answers)
        except MarkReadingError as err:
            logging.error(
                f'**Error: <ReadBoundingBoxSize - read_box_answer> :\
                     the read answers: {answers}. {err}')
        selected = rm.img_detect_answer(answers, ncol, nrow)
        rest = rm.translate_answer(selected, a_label, reverse=False)
        return {q_label[i]: rest[i] for i in range(len(rest))}
