"""Plotting images."""
import logging
from typing import Union
import numpy as np
import cv2
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle

logging.getLogger('matplotlib.font_manager').disabled = True


def plot_image(image: np.ndarray,
               title: str = 'Image',
               cmap_type: str = 'gray'):
    """
    Show image using matplotlib.

    Args:
        image (np.ndarray): image
        title (str, optional): title of the image. Defaults to 'Image'.
        cmap_type (str, optional): color map type. Defaults to 'gray'.
    """

    plt.figure(figsize=(8, 12))
    plt.imshow(image, cmap=cmap_type)
    plt.title(title)
    # plt.axis('off')
    plt.show()

    # contour plots


def show_contour(img: np.ndarray, cnt: list, title: str = 'Contours'):
    """
    Draw contour and print out to window using cv2 package.

    Args:
        img (np.ndarray): image
        cnt (list): contour list
        title (str, optional): plot title. Defaults to 'Contours'.
    """

    cv2.drawContours(img, cnt, -1, (1, 255, 0), 2)
    cv2.imshow(title, img)
    while True:
        k = cv2.waitKey(0)
        if k == -1:
            continue
        else:
            break
        cv2.destroyWindow(title)


def plot_contour(img: np.ndarray,
                 cnts: list,
                 title: str = 'Contours',
                 indv: Union[all, int] = all):
    """
    Plot contour using matlibplot.

    Args:
        img (np.ndarray): image
        cnts (list): list of contours
        title (str, optional): title of image. Defaults to 'Contours'.
        indv (Union[all, int], optional): select contour number to plot.
            Defaults to all.
    """

    plt.figure(figsize=(10, 12))
    plt.title(title)
    plt.imshow(img, cmap='gray')
    if indv is all:
        for cnt in cnts:
            r, _, c = cnt.shape
            cnt = np.reshape(cnt, (r, c))
            plt.plot(cnt[:, 0], cnt[:, 1], 'g-+')
    else:
        if isinstance(indv, int) and indv <= len(cnts):
            cnt = cnts[indv]
            r, _, c = cnt.shape
            cnt = np.reshape(cnt, (r, c))
            plt.plot(cnt[:, 0], cnt[:, 1], '-+')


def plot_contour_order(img: np.ndarray, cnts: list):
    """
    Plot contour with ordered marks using matplotlib.

    Args:
        img (np.ndarray): image
        cnts (list): list of contours
    """

    plt.figure(figsize=(10, 12))
    plt.imshow(img, cmap='gray')

    for i, cnt in enumerate(cnts):
        # cnt = cnts[i]
        r, _, c = cnt.shape
        cnt = np.reshape(cnt, (r, c))
        M = cv2.moments(cnt)
        cx = int(M['m10']/M['m00'])
        cy = int(M['m01']/M['m00'])
        plt.plot(cnt[:, 0], cnt[:, 1], '-o')
        plt.text(cx, cy, "#{}".format(i), color='green',
                 fontsize='large', fontweight='bold')
    plt.show()

# plot bounding box


def plot_bounding_box(img: np.ndarray, cnts: list):
    """
    Plotting bounding box.

    Args:
        img (np.ndarray): image
        cnts (list): list of contours
    """
    plt.figure(figsize=(10, 12))
    plt.imshow(img, cmap='gray')
    for c in cnts:
        x0, y0, w, h = cv2.boundingRect(c)
        x, y, w, h = float(x0), float(y0), float(w), float(h)
        currentAxis = plt.gca()
        currentAxis.add_patch(
            Rectangle((x, y), w, h, fill=False, linewidth=1, color='green'))
    plt.show()


def plot_bounding_box_order(img: np.ndarray,
                            cnts: list,
                            fontsize: str = 'small'):
    """
    Bounding box plot with ordered number.

    Args:
        img (np.ndarray): image
        cnts (list): list of contours
        fontsize (str, optional): fontsize. Defaults to 'small'.
    """
    plt.figure(figsize=(10, 12))
    plt.imshow(img, cmap='gray')
    for i, c in enumerate(cnts):
        # c = cnts[i]
        x0, y0, w, h = cv2.boundingRect(c)
        x, y, w, h = float(x0), float(y0), float(w), float(h)
        currentAxis = plt.gca()
        currentAxis.add_patch(
            Rectangle((x, y), w, h, fill=False, linewidth=1, color='green'))
        plt.text(x, y, f"{i}", color='green', fontsize=fontsize)
    plt.show()


def plot_img_hist(img: np.ndarray, title: str = 'histogram'):
    """
    Histogram plot

    Args:
        img (np.ndarray): image
        title (str, optional): title of the plot. Defaults to 'histogram'.
    """

    if len(img.shape) > 2:
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    plt.hist(img.ravel(), 256, [0, 256])
    plt.title(title)
    plt.show()
