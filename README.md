# Medical Record Reading - Optical Mark Reading 
## Getting Started

### Prerequisites
```
opencv_python>=4.2.0
numpy
imutils>=0.5.3
matplotlib
scipy
```
### installation
local install 
if you are inside the repo:
`pip install -e .`

 or from [testPyPI](https://test.pypi.org/project/mrr-omr/0.1.2/)  

## Running 

Built With
- python 3.7

## Documentation
Here is the link for the [documentation](https://hsiying.gitlab.io/datavac-omr/) of the library.

## Versioning 
verion 0.1.3

## Authors
- S.Huang

## License 

This project is licensed under the MIT License - see the LICENSE.md file for details

## Acknowledgement

 - Adrian Rosebrock for inspirations 
